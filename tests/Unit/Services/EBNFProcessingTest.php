<?php

namespace Tests\Unit\Services;

use App\Services\ProcessingServices\EBNFProcessing;
use Illuminate\Support\Facades\Storage;

class EBNFProcessingTest extends \Tests\TestCase
{
    private $file = 'testFile.txt';

    public function setUp(): void
    {
        parent::setUp();

        $content = <<<EOT
        Grain and Leaf;E32NY;100
        Grain salad;nuts;12h

        Ghana Kitchen;NW42QA;40
        Premium meat selection;;36h
        Breakfast;gluten,eggs;12h
        EOT;

        Storage::put($this->file, $content);
    }

    public function test_i_can_process_an_ebnf_file_into_collection()
    {
        $service = new EBNFProcessing();

        $collection = $service->process($this->file);
        $expected = [
            [
                'name' => 'Grain and Leaf',
                'postcode' => 'E32NY',
                'covers' => 100,
                'dishes' => [
                    [
                        'name' => 'Grain salad',
                        'allergies' => [
                            'nuts'
                        ],
                        'noticePeriod' => 12
                    ]
                ]
            ],
            [
                'name' => 'Ghana Kitchen',
                'postcode' => 'NW42QA',
                'covers' => 40,
                'dishes' => [
                    [
                        'name' => 'Premium meat selection',
                        'allergies' => [],
                        'noticePeriod' => 36
                    ],
                    [
                        'name' => 'Breakfast',
                        'allergies' => [
                            'gluten',
                            'eggs',
                        ],
                        'noticePeriod' => 12
                    ],
                ]
            ]
        ];

        $this->assertEquals(collect($expected), $collection);
    }

    public function tearDown(): void
    {
        Storage::delete($this->file);
        parent::tearDown();
    }
}
