<?php

namespace Tests\Unit\Services;

use App\Services\EBNFProcessing;
use App\Services\SearchService;
use Illuminate\Support\Facades\Storage;

class SearchServiceTest extends \Tests\TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function test_i_get_a_breakfast_dish_for_ghana_kitchen_if_search_criteria_is_within_24_hours_for_NW42QA()
    {
        $service = new SearchService();

        $restaurants = collect([
            [
                'name' => 'Grain and Leaf',
                'postcode' => 'E32NY',
                'covers' => 100,
                'dishes' => [
                    [
                        'name' => 'Grain salad',
                        'allergies' => [
                            'nuts'
                        ],
                        'noticePeriod' => 12
                    ]
                ]
            ],
            [
                'name' => 'Ghana Kitchen',
                'postcode' => 'NW42QA',
                'covers' => 40,
                'dishes' => [
                    [
                        'name' => 'Premium meat selection',
                        'allergies' => [],
                        'noticePeriod' => 36
                    ],
                    [
                        'name' => 'Breakfast',
                        'allergies' => [
                            'gluten',
                            'eggs',
                        ],
                        'noticePeriod' => 12
                    ],
                ]
            ],
        ]);

        $criteria = [
            "filename" => "ll",
            "day" => now()->addDay()->format('d/m/Y'),
            "time" => '11:00',
            "location" => "NW42QA",
            "covers" => "40",
        ];

        $collection = $service->getMenuItems($criteria, $restaurants);
        $expected = collect([
            [
                'name' => 'Breakfast',
                'allergies' => [
                    'gluten',
                    'eggs',
                ],
                'noticePeriod' => 12
            ]
        ]);

        $this->assertEquals($expected->first()['name'], $collection->first()['name']);
        $this->assertEquals($expected->first()['allergies'], $collection->first()['allergies']);
    }

    public function test_i_get_a_both_dishes_for_ghana_kitchen_if_search_criteria_is_within_4_days_for_NW42QA()
    {
        $service = new SearchService();

        $restaurants = collect([
            [
                'name' => 'Grain and Leaf',
                'postcode' => 'E32NY',
                'covers' => 100,
                'dishes' => [
                    [
                        'name' => 'Grain salad',
                        'allergies' => [
                            'nuts'
                        ],
                        'noticePeriod' => 12
                    ]
                ]
            ],
            [
                'name' => 'Ghana Kitchen',
                'postcode' => 'NW42QA',
                'covers' => 40,
                'dishes' => [
                    [
                        'name' => 'Premium meat selection',
                        'allergies' => [],
                        'noticePeriod' => 36
                    ],
                    [
                        'name' => 'Breakfast',
                        'allergies' => [
                            'gluten',
                            'eggs',
                        ],
                        'noticePeriod' => 12
                    ],
                ]
            ],
        ]);

        $criteria = [
            "filename" => "ll",
            "day" => now()->addDays(4)->format('d/m/Y'),
            "time" => '11:00',
            "location" => "NW42QA",
            "covers" => "40",
        ];

        $collection = $service->getMenuItems($criteria, $restaurants);
        $expected = collect([
            [
                'name' => 'Premium meat selection',
                'allergies' => [],
                'noticePeriod' => 36
            ],
            [
                'name' => 'Breakfast',
                'allergies' => [
                    'gluten',
                    'eggs',
                ],
                'noticePeriod' => 12
            ]
        ]);

        $this->assertEquals($expected->first()['name'], $collection->first()['name']);
        $this->assertEquals($expected->first()['allergies'], $collection->first()['allergies']);

        $this->assertEquals($expected->last()['name'], $collection->last()['name']);
        $this->assertEquals($expected->last()['allergies'], $collection->last()['allergies']);
    }
}
