# CiPa - Backend/PHP Coding Challenge

CP needs a program to search its database of vendors for menu items available given day, time, location and a headcount. Each vendor has a name, postcode and a maximum headcount it can serve at any time. Menu items each have a name, list of allergies and notice period needed for placing an order.

## Requirements

Your task is to write a console application that takes five input parameters:

```
filename - input file with the vendors data
day      - delivery day (dd/mm/yy)
time     - delivery time in 24h format (hh:mm)
location - delivery location (postcode without spaces, e.g. NW43QB)
covers   - number of people to feed
```
 
and prints a list of item names and item-specific allergens available to order given the following rules:

1. Vendor must be able to deliver to the requested location, e.g. vendor with a postcode starting "NW" can only deliver to a postcode starting with "NW", etc.
2. Vendor must be able to serve the requested number of covers
3. Item notice period must be less or equal to the difference between the search time and the actual time of the delivery

The input file is provided in the following EBNF-like text format:
    
```
vendors      = { vendor, new line, items, new line } EOF
vendor       = name, ";", postcode, ";", max covers
items        = { item, new line }
item         = name, ";", allergies, ";", advance time
name         = /[A-Za-z ]*/
postcode     = /[A-Za-z][A-Za-z0-9]*/
max covers   = /\d*/
advance time = /\d\dh/
new line     = "\r\n"
```

You may use [this example input file](./example-input).    

If today is *10/11/18, 12 AM* then calling your application with the example input and the parameters `11/11/18 11:00 NW43QB 20` should print the following lines:

```
Breakfast;gluten,eggs
````
    
On the same day, calling your application with the parameters `14/11/18 11:00 NW43QB 20` should print the following lines:

```
Premium meat selection;;
Breakfast;gluten,eggs
``` 

## SETUP

* run `composer install`
* run `cp .env.example .env`
* run `php artisan key:generate`
* run `vendor/bin/phpunit` to run tests (including test data ^ )
* If you would like to run the console command yourself please do this:
  * run `mkdir storage/app/Inputs/`
  * run `cp example.txt storage/app/Inputs/example.txt`
  * run `php artisan z:item-availability example.txt 14/02/2020 11:00 NW42QA 20` changing the date to be one day AFTER your current day, to test it prints same output as above 
  * run `php artisan z:item-availability example.txt 18/02/2020 11:00 NW42QA 20` changing the date to be four days AFTER your current day, to test it prints same output as above 
  * you can run the command without giving the inputs and the command will then hopefully ask you to input each param
  
Command itself is found in app/Console/Commands. 
Services are in app/Services/
Tests in tests/   
  
## My Thoughts

For production, we should probably store this in MySQL with indexes otherwise we can definitely store the processed file into JSON and use that instead of re-processing the file if it exists (some checks like file size/name will need to be made to make that decision). If Using MySQL, we can also denormalise or create a view so that we can do all the checks on dishes instead of doing multiple queries. You could also have different database (sharding) so all postcodes of NW* are in one so you only query those restaurants in the area rather than the whole database. using Redis/Caching will also improve the time.
  

## Additional Notes

Structure your code as if this was a real, production application. You may however choose to provide simplified implementations for some aspects (e.g. in-memory persistence instead of a full database if you think any persistence is required at all).

State any assumptions you make as comments in the codebase. If any aspects of the above requirements are unclear then please also state, as comments in the source, your interpretation of the requirement.

The purpose of the exercise is to evaluate your approach to software development covering among other things elements of Object Oriented Design, Software Design Patterns and Testing. This exercise is not time bounded.

Complete the exercise in PHP. You are free to implement any mechanism for feeding input into your solution (for example, using hard coded data within unit test). You should provide sufficient evidence that your solution is complete by, as a minimum, indicating that it works correctly against the supplied test data.

Please, do not use any external libraries to solve this problem, but you may use external libraries or tools for building or testing purposes. Specifically, you are encouraged to use PHPUnit to test your code.
