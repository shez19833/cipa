<?php

namespace App\Providers;

use App\Services\ProcessingServices\EBNFProcessing;
use App\Services\ProcessingServices\ProcessingInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ProcessingInterface::class, EBNFProcessing::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
