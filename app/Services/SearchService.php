<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Collection;

class SearchService
{
    public function getMenuItems(array $criteria, Collection $restaurants) : Collection
    {
        $matchedRestaurants = $restaurants->filter(function ($restaurant) use ($criteria) {
          return $this->checkRestaurantMatchesCriteria($restaurant, $criteria);
        });

        return $matchedRestaurants
            ->pluck('dishes')
            ->flatten(1) // 1 = depth, otherwise it would flatten sub sub arrays which is not what i want
            ->filter(function ($dish) use ($criteria) {
               return $this->checkDishHasEnoughNotice($dish, $criteria);
            });
    }

    private function checkRestaurantMatchesCriteria($restaurant, $criteria)
    {
        return $restaurant['covers'] >= $criteria['covers']
            && $restaurant['postcode'] == $criteria['location']; // TODO should have called it location :(
    }

    private function checkDishHasEnoughNotice($dish, $criteria)
    {
        // TODO could also add the 'date time a dish can be ordered for ie now()->addHours.. on to the dish itself whilst processing'
        $foodRequiredAt = Carbon::createFromFormat('d/m/Y H:i', $criteria['day'] . ' ' . $criteria['time']);
        $noticePeriodForDish = now()->addHours($dish['noticePeriod']);

        return $foodRequiredAt > $noticePeriodForDish;
    }
}
