<?php

namespace App\Services\ProcessingServices;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class EBNFProcessing implements ProcessingInterface
{
    // TODO could potentially leverage an external library that may parse EBNF Files? didnt search as wasnt allowed
    public function process(string $filename): Collection
    {
        $data = [];
        $contents = explode("\n\n", Storage::get($filename));

        foreach ($contents as $info) {
            $lines = explode("\n", $info);
            $restaurant = explode(';', array_shift($lines));

            $array = [
                'name' => $restaurant[0],
                'postcode' => $restaurant[1],
                'covers' => (int) $restaurant[2],
            ];

            foreach ($lines as $line) {
                $dish = explode(';', $line);

                if (count($dish) == 1) { // for the new line at the end of the file
                    continue;
                }

                $array['dishes'][] = [
                    'name' => $dish[0],
                    'allergies' => $dish[1] == '' ? [] : explode(',', $dish[1]),
                    'noticePeriod' => substr($dish[2], 0, -1),
                ];
            }

            $data[] = $array;
        }

        return collect($data);
    }
}
