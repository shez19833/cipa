<?php

namespace App\Services\ProcessingServices;

use Illuminate\Support\Collection;

interface ProcessingInterface
{
    public function process(string $filename);
}
