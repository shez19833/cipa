<?php

namespace App\Console\Commands;

use App\Services\ProcessingServices\ProcessingInterface;
use App\Services\SearchService;
use Illuminate\Console\Command;

class ItemAvailabilityCommand extends Command
{
    protected $signature = 'z:item-availability
        {filename? : Filename stored in storage/app/inputs directory}
        {day? : Delivery day (dd/mm/yy)}
        {time?} : Delivery Time 24h format (hh:mm)
        {location? : Postcode without spaces}
        {covers? : Number of people to feed}';

    protected $description = 'Command to return item that can be ordered based on criteria provided';

    private $processingService;
    private $restaurants;
    private $searchCriteria = [];
    private $searchService;

    public function __construct(ProcessingInterface $processingService, SearchService $searchService)
    {
        parent::__construct();

        $this->processingService = $processingService;
        $this->searchService = $searchService;
    }

    public function handle()
    {
        $this->searchCriteria = $this->getMissingSearchCriteria();
        $this->restaurants = $this->processingService->process('inputs/' . $this->searchCriteria['filename']);
        $dishes = $this->searchService->getMenuItems($this->searchCriteria, $this->restaurants);
        $this->displayOutput($dishes);

        // TODO Optional:  potentially saving the file as JSON and then re-read that instead of input file if the name is the same
    }

    private function getMissingSearchCriteria() : array
    {
        $inputs = $this->arguments();
        array_shift($inputs); //  rid of command itself arguement

        foreach ($inputs as $key => $value)
        {
            if (!$value) {
                // TODO Optional could tailor the information to each input
                // TODO Validation could then also be done
                $inputs[$key] = $this->ask('Please let us know about missing info: ' . $key);
            }
        }

        return $inputs;
    }

    // TODO this could be another external file called DISPLAY with several functions i.e. table, json, html etc..
    // Or we could have TableOutput implements OutputInterface...
    private function displayOutput($dishes) : void
    {
        $dishes = $dishes->map(function($dish){
           $dish['allergies'] = implode(',', $dish['allergies']);
           unset($dish['noticePeriod']);
           return $dish;
        });

        $headers = ['Dish', 'Allergy information'];
        $this->table($headers, $dishes);
    }
}
